# QA strategy challenge

This assessment has two main tasks. If you cannot fulfill task no. 2, please contact us and we will provide an alternative.

## First task

Your first task is to create a test strategy for our IBE (Internet Booking Engine) platform available for testing at https://ferry.wemovo.com/en/start/. Consider the scenario where you have very limited QA resources (lack of time and developers):

- Which are the most important features to test? 
- How would you define a problematic release and which aspects would you focus on to avoid it?

Create an outline of features you would test, focusing on good coverage rather than a comprehensive list of test cases. Additionally, provide a concise explanation of your prioritization.

### What we'll be evaluating

The goal is to demonstrate your ability to approach platform testing strategically, rather than to present a comprehensive list of test cases. We want to see how you prioritize and *why* -- understanding your reasoning is very important for us.

## Second task

Your second task is to test the **booking flow**. Using the route Milazzo->Panarea and the test credit card	(Name: Test Number: 4012000300001003 Expiration Date: 04/2020 CCV: 003) you can complete the order.

- Write a list of test cases, with an emphasis on good coverage.
- Prioritize these test cases according to their business impact and state your reasons concisely for doing so;
- Create a solution for automating these test cases; you can use a language and framework of your choice;
- Provide a short explanation for your automation solution, plus some instructions on how we can run it ourselves.

### What we'll be evaluating

The goal here is to see how autonomous you are and how much you've made your internal switch to think in "automated mode" rather than "manual testing mode". The choice of technologies is not important for us -- you can choose whatever you feel comfortable with. But we'll be looking in detail into how you design the test cases and how easily we can pick up your work and build on top of it.

## Delivery

Once you're done send us a pull request or your solutions via email.

## Talk with us

We're more than happy to give you support and disucss any problem you have along the way. If you have questions don't hesitate in approaching us!
